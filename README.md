## Blockchaindemo control panel ##
### How to install & run ###
- Install Node.JS (https://nodejs.org/en/). This program was developed against version 6.11.x.
- Install MongoDB server.  

- Set following environment variables: TAKE_XEM and TAKE_MOSAIC.
	- TAKE_XEM should contain private key for wallet where from XEMs are transfered to new users.
	- TAKE_MOSAIC should contain private key for wallet where from 'kwh' Mosaics are transfered to new users.  

- Install dependencies running command 'npm install' in root folder of control panel.
- Start application running command 'npm start' in root folder of control panel.

### Heroku ###
http://blockchaindemoweb.herokuapp.com/