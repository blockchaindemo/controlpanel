import User from '../users/mongo_schemas/User';
import { transferMosaics, endpoint, takeXEMsFrom, takeMosaicsFrom, initialXEMsAmount } from '../utils/nem.js';
import nem from 'nem-sdk';

export const handleUserLogin = (req, res) => {
  var username = req.body.username;
  var pass = req.body.password;
  const user = User.findOne({username: username, password: pass}).exec((err, result) => {
    console.log('err', err);
    console.log('result', result);

    if(result == null || !result) {
      return res.json({error: {errormsg: "Invalid username or password"}});
    } else {
      return res.json({user: result});  
    }
  })
}

export const handleUserRegister = (req, res) => {
  console.log('handleUserRegister');

  var username = req.body.username;
  var pass = req.body.password;
  var isproducer = req.body.isproducer;

  var query = User.findOne({username: username});

  //console.log('query', query);

  let userFound = false;
  if(!userFound) {

    // nem wallet and object creating
    var wallet = nem.model.wallet.createBrain(username, pass, nem.model.network.data.testnet.id);
    var common = nem.model.objects.create("common")(pass, "");
    // Get the wallet account to decrypt
    var walletAccount = wallet.accounts[0];
    // Decrypt account private key
    nem.crypto.helpers.passwordToPrivatekey(common, walletAccount, walletAccount.algo);
    // The common object now has a private key

    // create and save user model via mongoose
    User.create({
      username: username,
      password: pass,
      isproducer: isproducer,
      nemWallet: wallet,
      commonObject: common
      }, function (err, user) {
      if (err) {
        console.log("error creating new user");
        return res.json({error: {errormsg: "Error occured on registration"}});
      } else {
        // Transfer that account some XEMs
        // Create a common object holding key
        var commonFrom = nem.model.objects.create("common")("", takeXEMsFrom);
        // Create an un-prepared transfer transaction object
        var transferTransaction = nem.model.objects.create("transferTransaction")(walletAccount.address, !!isproducer ? initialXEMsAmount[0] : initialXEMsAmount[1], "Initial XEMs");
        // Prepare the transfer transaction object
        var transactionEntity = nem.model.transactions.prepare("transferTransaction")(commonFrom, transferTransaction, nem.model.network.data.testnet.id);
        // Serialize transfer transaction and announce
        nem.model.transactions.send(commonFrom, transactionEntity, endpoint);

        // Also transfer initial kwh mosaics if this account belongs to producer
        if (!!isproducer) {
          transferMosaics(takeMosaicsFrom, walletAccount.address, "blockchaindemo1.demogrid", "kwh", 1000000, "Mosaic transfer for producer: " + username);
        }

        console.log("created new user");
        return res.json({user: user});
      }
    });
  } else {
    res.send({
      error: "That username already exists!"
    });
  }
}