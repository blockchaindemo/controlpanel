import User from '../users/mongo_schemas/User';

export const handleGetPrices = (req, res) => {
  console.log('Cost request for user: ' + req.query._id);
  const user = User.findOne({_id: req.query._id}).exec((err, result) => {
    if(err || result == null || !result) {
      return res.json({error: true});
    } else {
      return res.json({
        success: "true",
        sellprice: result.sellprice,
        buyprice: result.buyprice,
        averagePricePerEnergy: 0.12
      });
    }
  });
}

export const handleSetBuyPrice = (req, res) => {
  console.log('New payment setting for user: ' + req.body._id + '. New buy price is ' + req.body.pricePerEnergy);
  User.findByIdAndUpdate({ _id: req.body._id }, { $set: { buyprice: req.body.pricePerEnergy }}, (err, result) => {
    if (err) {
      return res.json({error: err});
    } else {
      res.send({
        success: "true",
        buyprice: req.body.pricePerEnergy
      });
    }
  });
}

export const handleSetSellPrice = (req, res) => {
  console.log('New payment setting for user: ' + req.body._id + '. New sell price is ' + req.body.pricePerEnergy);
  User.findByIdAndUpdate({ _id: req.body._id }, { $set: { sellprice: req.body.pricePerEnergy }}, (err, result) => {
    if (err) {
      return res.json({error: err});
    } else {
      res.send({
        success: "true",
        sellprice: req.body.pricePerEnergy
      });
    }
  });
}