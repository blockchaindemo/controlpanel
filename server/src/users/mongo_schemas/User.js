var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  },
  isproducer: {
    type: Boolean,
    required: true
  },
  sellprice: {
    type: Number,
    default: 0.10
  },
  buyprice: {
    type: Number,
    default: 0.11
  },
  nemWallet: {
    type: Object,
    required: true
  },
  commonObject: {
    type: Object,
    required: true
  },
  produced: {
    type: Number,
    required: false,
    default: 0.0
  },
  consumed: {
    type: Number,
    required: false,
    default: 0.0
  }
});
// Virtual for author's URL
UserSchema
.virtual('url')
.get(function () {
  return '/users/' + this._id;
});

var User = mongoose.model('User', UserSchema);
export default User;