const express = require('express');
var bodyParser = require('body-parser')
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
import User from './users/mongo_schemas/User';

import { handleUserLogin, handleUserRegister, createDummyUser } from './users/userFunctions';
import { handleGetPrices, handleSetBuyPrice, handleSetSellPrice } from './users/priceFunctions';
import { setMeterIn, setMeterOut } from './smartmeter/readingFunctions';
import { generateInvoices, getInvoices } from './invoices/invoiceFunctions';

const app = express();

//connect to db
const uri = process.env.MONGODB_URI || 'mongodb://localhost:27017/data';

mongoose.connect(uri, { useMongoClient: true })

//bodyparser
app.use(express.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.set('port', (process.env.PORT || 3001));

if (process.env.NODE_ENV === 'production') {
  console.log("Running in production mode");
  app.use(express.static(__dirname + '/../../client/build'));
}

app.get('/', (request, response) => {
  response.sendFile(__dirname + '/../client/build/index.html');
});

app.listen(app.get('port'), () => {
  console.log(`BlockchainDemo server is running on port: ${app.get('port')}`);
});

app.post('/register', handleUserRegister);
app.post('/login', handleUserLogin);

app.get('/price', handleGetPrices);
app.put('/price/buy', handleSetBuyPrice);
app.put('/price/sell', handleSetSellPrice);

app.put('/meter/in', setMeterIn);
app.put('/meter/out', setMeterOut);

app.get('/geninvoices', generateInvoices);
app.get('/getinvoices', getInvoices);

// for testing users
app.get('/getallusers', (req, res) => {
  User.find().exec((err, users) => {
    if(err) return res.json({error: true})
    return res.json({users: users});
  })
});

app.get('/producers', (request, respond) => {
  console.log('Producers request for user: ' + request.query._id);
  const user = User.find({isproducer: true})
  .exec((err, res) => {
    const producerList = [];
    if (err || !res) {
      respond.send({
        error: err
      });
    } else {
      res.forEach((t, i) => {
        producerList.push({
          producerId: i,
          username: t.username,
          produced: t.produced,
          consumed: t.consumed,
          sellprice: t.sellprice
        });
      });
      respond.send({
        success: "true",
        producerList: producerList
      });
    }
  });
});