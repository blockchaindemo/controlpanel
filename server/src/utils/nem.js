import User from '../users/mongo_schemas/User';
import nem from 'nem-sdk';

const endpoint_ = nem.model.objects.create("endpoint")(process.env.NIS_ADDR || "http://23.228.67.85", process.env.NIS_PORT || "7890");

const takeXEMsFrom_ = process.env.TAKE_XEM || "";
const takeMosaicsFrom_ = process.env.TAKE_MOSAIC || "";
const initialXEMsAmount_ = [30, 1];
//const initialXEMsAmount_ = [1, 1];

export const endpoint = endpoint_;
export const takeXEMsFrom = takeXEMsFrom_;
export const takeMosaicsFrom = takeMosaicsFrom_;
export const initialXEMsAmount = initialXEMsAmount_;

export const transferMosaics = (from, toAddr, namespace, mosaic, amount, message) => {
  // Create a common object holding key
  var common = nem.model.objects.create("common")("", from);
  // Create variable to store our mosaic definitions, needed to calculate fees properly (already contains xem definition)
  var mosaicDefinitionMetaDataPair = nem.model.objects.get("mosaicDefinitionMetaDataPair");
  // Create an un-prepared mosaic transfer transaction object (use same object as transfer tansaction)
  var transferTransaction = nem.model.objects.create("transferTransaction")(toAddr, 1, message);
  // Create a mosaic attachment object
  var mosaicAttachment = nem.model.objects.create("mosaicAttachment")(namespace, mosaic, amount);
  // Push attachment into transaction mosaics
  transferTransaction.mosaics.push(mosaicAttachment);

  nem.com.requests.namespace.mosaicDefinitions(endpoint_, mosaicAttachment.mosaicId.namespaceId).then(function(res) {
    // Look for the mosaic definition(s) we want in the request response
    var neededDefinition = nem.utils.helpers.searchMosaicDefinitionArray(res.data, [mosaic]);
    // Get full name of mosaic to use as object key
    var fullMosaicName  = nem.utils.format.mosaicIdToName(mosaicAttachment.mosaicId);
    // Check if the mosaic was found
    if(undefined === neededDefinition[fullMosaicName]) {
      console.log("Mosaic not found !");
      return;
    }
    // Set kwh mosaic definition into mosaicDefinitionMetaDataPair
    mosaicDefinitionMetaDataPair[fullMosaicName] = {};
    mosaicDefinitionMetaDataPair[fullMosaicName].mosaicDefinition = neededDefinition[fullMosaicName];

    // Prepare the transfer transaction object
    var transactionEntity = nem.model.transactions.prepare("mosaicTransferTransaction")(common, transferTransaction, mosaicDefinitionMetaDataPair, nem.model.network.data.testnet.id);
    // Serialize transfer transaction and announce
    nem.model.transactions.send(common, transactionEntity, endpoint_)
  },
  function(err) {
    console.error(err);
  });
};