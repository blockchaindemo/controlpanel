import User from '../users/mongo_schemas/User';
import { transferMosaics, endpoint, takeXEMsFrom, takeMosaicsFrom, initialXEMsAmount } from '../utils/nem.js';
import nem from 'nem-sdk';

export const generateInvoices = (request, response) => {
  const invoices = [];
  const user = User.find({isproducer: true})
    .where('produced').gt(0.0)
    .sort({sellprice: 1})
    .exec((err, res) => {
      if(err || !res) {
        return response.json({error: err});
      } else {
        let producers = res.map((producer) => {
          return new Promise((resolve) => {
            let producedEnergy = producer.produced;
            User.find()
            .where('consumed').gt(0.0)
            .sort({consumed: 1})
            .where('buyprice').gte(producer.sellprice)
            .exec((err2, res2) => {
              if(err2 || !res2) {
                return response.json({error: err2});
              } else {
                const consumersCount = res2.length;
                if (consumersCount > 0) {
                  let sumConsumed = res2.reduce((a, b) => a.consumed + b.consumed);
                  res2.forEach((consumer, index) => {
                    const freeEnergyForOne = sumConsumed > producedEnergy ? producedEnergy / (consumersCount - index) : producedEnergy;

                    const consumeFromThisProducer = Math.min(freeEnergyForOne, consumer.consumed);

                    transferMosaics(
                      producer.commonObject.privateKey,
                      consumer.nemWallet.accounts[0].address,
                      "blockchaindemo1.demogrid",
                      "kwh",
                      consumeFromThisProducer * 1000.0,
                      JSON.stringify({producer: producer.username, consumer: consumer.username, price: producer.sellprice})
                    );

                    // For debugging
                    invoices.push({
                      producer: producer.username,
                      consumer: consumer.username,
                      price: producer.sellprice,
                      quantity: consumeFromThisProducer,
                      fromAddress: producer.nemWallet.accounts[0].address,
                      toAddress: producer.nemWallet.accounts[0].address
                    });

                    const leftoverEnergyToBeBilled = Math.max(consumer.consumed - consumeFromThisProducer, 0.0);
                    User.findByIdAndUpdate({ _id: consumer._id }, { $set: { consumed: leftoverEnergyToBeBilled }}, (error, result) => {
                      if (error) console.error(error);
                    });
                    sumConsumed -= consumeFromThisProducer;
                    producedEnergy -= consumeFromThisProducer;
                    User.findByIdAndUpdate({ _id: producer._id }, { $set: { produced: producedEnergy }}, (error, result) => {
                      if (error) console.error(error);
                    });
                  });
                }
                resolve();
              }
            });
          });
        });
        Promise.all(producers)
          .then(() => {
            response.send({
              success: "true",
              invoices: invoices
            });
          })
          .catch((err) => {
            return response.json({error: err});
          });
      }
    });
};

export const getInvoices = (request, response) => {
  var query = User.findOne({_id: request.query._id})
    .exec((err, res) => {
      const invoices = [];
      // Get incoming transactions
      nem.com.requests.account.transactions.incoming(endpoint, res.nemWallet.accounts[0].address).then(function(res) {
        res.data.forEach(data => {
          const mosaics = data.transaction.mosaics;
          const message = data.transaction.message;
          const utf8message = nem.utils.format.hexMessage(message);
          try {
            const JSONmessage = JSON.parse(utf8message);

            let isKWHTransaction = false;
            let KWHMosaic = undefined;
            mosaics.forEach((t) => { if (t.mosaicId.name === "kwh") {isKWHTransaction = true; KWHMosaic = t} });
            if (KWHMosaic) {
              const invoice = {
                mosaic: KWHMosaic,
                message: JSONmessage
              };
              invoices.push(invoice);
            }
          } catch(e) {
            console.log("Invalid message structure found inside transaction for user " + request.query._id + ".: " + e);
          }
        });
        response.send({
          success: true,
          invoices: invoices
        });
      }, function(err) {
        response.send({
          error: err
        });
      });
    });
};