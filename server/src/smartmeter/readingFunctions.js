import User from '../users/mongo_schemas/User';

export const setMeterIn = (req, res) => {
  User.findByIdAndUpdate({ _id: req.body._id }, { $set: { consumed: req.body.meterValue }}, (err, result) => {
    if (err) {
      return res.json({error: err});
    } else {
      res.send({
        success: "true",
        consumed: req.body.meterValue
      });
    }
  });
};

export const setMeterOut = (req, res) => {
  User.findByIdAndUpdate({ _id: req.body._id }, { $set: { produced: req.body.meterValue }}, (err, result) => {
    if (err) {
      return res.json({error: err});
    } else {
      res.send({
        success: "true",
        produced: req.body.meterValue
      });
    }
  });
};