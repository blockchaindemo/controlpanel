import React from 'react';
import PropTypes from 'prop-types';
import LoginPageContainer from '../containers/LoginPageContainer';
import ControlPanel from './ControlPanel';

const Page = ({loading, loggedIn}) => (
  <div>
    {(!loading &&
    ((!loggedIn &&
      <LoginPageContainer />
    ) || (
      <ControlPanel />
    )))}
  </div>
);

Page.propTypes = {
  loggedIn: PropTypes.bool.isRequired
}

export default Page;
