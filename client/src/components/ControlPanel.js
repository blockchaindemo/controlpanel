import React, { Component } from 'react';
import { Tabs, Tab } from 'react-bootstrap';

import PaymentControlContainer from '../containers/PaymentControlContainer';
import ProducerListContainer from '../containers/ProducerListContainer';
import SettingsContainer from '../containers/SettingsContainer';
import InvoicesContainer from '../containers/InvoicesContainer';

class ControlPanel extends Component {
  render() {
    return (
      <div className="ControlPanel">
        <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
          <Tab eventKey={1} title="Osta"><PaymentControlContainer /></Tab>
          <Tab eventKey={2} title="Sähköntuottajat"><ProducerListContainer /></Tab>
          <Tab eventKey={3} title="Sähkömittari"><SettingsContainer /></Tab>
          <Tab eventKey={4} title="Laskut"><InvoicesContainer /></Tab>
        </Tabs>
      </div>
    );
  }
}

export default ControlPanel;
