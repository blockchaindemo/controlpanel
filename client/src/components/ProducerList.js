import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import PropTypes from 'prop-types';

class ProducerList extends Component {
  componentWillMount() {
    this.props.fetchProducerList();
  }
  componentDidMount() {
    this.interval = setInterval(this.update, 5000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  update = () => {
    this.props.fetchBackground();
  }
  render() {
    return (
      <div className="ProducerList">
        {!this.props.loading &&
          <Grid>
            <Row className="show-grid">
              <Col md={12} sm={12} xs={12}>
                <h1 className="center">Sähköntuottajat ja hinnat</h1>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col md={12} sm={12} xs={12}>
                <Table bordered condensed hover>
                  <thead>
                    <tr>
                      <th>Nimi</th>
                      <th>Hinta per kWh</th>
                      <th>Tuottanut</th>
                    </tr>
                  </thead>
                  {!!this.props.producerList &&
                    this.props.producerList.map((producer) => {
                      return (
                        <tbody key={producer.producerId}>
                          <tr>
                            <td>{producer.username}</td>
                            <td>{producer.sellprice}</td>
                            <td>{producer.produced} kWh</td>
                          </tr>
                        </tbody>
                      )
                    })
                  }
                </Table>
              </Col>
            </Row>
          </Grid>
        }
      </div>
    );
  }
}

ProducerList.propTypes = {
  fetchProducerList: PropTypes.func,
  fetchBackground: PropTypes.func,
  producerList: PropTypes.array,
  loading: PropTypes.bool.isRequired
}

export default ProducerList;
