import React, { Component } from 'react';
import { Grid, Row, Col, Button, Checkbox, Modal } from 'react-bootstrap';
import { FormGroup, FormControl } from 'react-bootstrap';
import PropTypes from 'prop-types';

class LoginPage extends Component {
  constructor(props) {
      super(props);
      this.state = {lusername: "", lpassword: "", rusername: "", rpassword: "", wantsell: false,
      showModal: false
    };
  }

  close = () => {
    this.setState({ showModal: false });
  }

  openModal = () => {
    this.setState({ showModal: true });
  }

  handlelUsernameChange = (event) => {
    this.setState({lusername: event.target.value});
  }

  handlelPasswordChange = (event) => {
    this.setState({lpassword: event.target.value});
  }

  handleLoginClick = (event) => {
    this.props.loginClick(this.state.lusername, this.state.lpassword);
    event.preventDefault();

  }

  handlerUsernameChange = (event) => {
    this.setState({rusername: event.target.value});
  }

  handlerPasswordChange = (event) => {
    this.setState({rpassword: event.target.value});
  }

  handleWantSellCheck = (event) => {
    this.setState({wantsell: !this.state.wantsell});
  }

  handleRegisterClick = (event) => {
    this.props.registerClick(this.state.rusername, this.state.rpassword, this.state.wantsell);
    event.preventDefault();
  }
  
  componentDidUpdate (prevProps, prevState) {
    if(prevProps.fetching && !this.props.fetching && this.props.loginOrRegisterState !== "") {
      console.log('lol');
      this.openModal();
    }
  }

  render() {
    const { loginOrRegisterState, fetching } = this.props;

    if(fetching) {
      return <div className="loadingIndicator"></div>
    }

    return (
      <div className="LoginPage">
        <Grid>
          <Row className="show-grid">
            <Col md={6} sm={6} xs={12}>
              <center>
                <form onSubmit={this.handleLoginClick}>
                  <Button type="submit" value="Submit" bsStyle="primary" bsSize="large" >Kirjaudu</Button>
                  <FormGroup
                    controlId="loginForm"
                  >
                    <FormControl
                      type="text"
                      placeholder="Käyttäjätunnus"
                      value={this.state.username} onChange={this.handlelUsernameChange}
                    />
                    <FormControl
                      type="password"
                      placeholder="Salasana"
                      value={this.state.password} onChange={this.handlelPasswordChange}
                    />
                  </FormGroup>
                </form>
              </center>
            </Col>
            <Col md={6} sm={6} xs={12}>
              <center>
                <form onSubmit={this.handleRegisterClick}>
                  <Button type="submit" value="Submit" bsSize="large">Rekisteröidy</Button>
                  <Modal show={this.state.showModal} onHide={this.close} onClick={this.handleModalClick}>
                    <Modal.Header closeButton>
                    {/*<Modal.Title>Modal heading</Modal.Title>*/}
                    </Modal.Header>
                    <Modal.Body >
                      {loginOrRegisterState === "register_fail" ? `Failed to register`:
                      loginOrRegisterState === "register_success" ? "Thanks for registering!":
                      loginOrRegisterState === "login_fail" ? "Failed to login":
                      loginOrRegisterState === "login_success" ? "Thanks for logging in!"
                      : ""}  
                    </Modal.Body>
                    <Modal.Footer>
                    <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                  </Modal>

                  <FormGroup
                    controlId="registerForm"
                  >
                    <FormControl
                      type="text"
                      placeholder="Käyttäjätunnus"
                      value={this.state.rusername} onChange={this.handlerUsernameChange}
                    />
                    <FormControl
                      type="password"
                      placeholder="Salasana"
                      value={this.state.rpassword} onChange={this.handlerPasswordChange}
                    />
                    <Checkbox bsSize="large" value={this.state.wantsell} onChange={this.handleWantSellCheck}>
                      Haluan myydä sähköä
                    </Checkbox>
                  </FormGroup>
                </form>
              </center>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

LoginPage.propTypes = {
  loginClick: PropTypes.func.isRequired,
  registerClick: PropTypes.func.isRequired,
  registerSuccess: PropTypes.bool,
  loading: PropTypes.bool
}

export default LoginPage;
