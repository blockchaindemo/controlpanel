import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { FormGroup, FormControl, HelpBlock } from 'react-bootstrap';
import PropTypes from 'prop-types';

class SettingsPage extends Component {
  constructor(props) {
      super(props);
      this.state = {
        fetching: false,
        isError: false,
        produced: this.props.produced,
        consumed: this.props.consumed
      };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.consumed !== nextProps.consumed || this.state.produced !== nextProps.produced) {
      this.setState({consumed: nextProps.consumed, produced: nextProps.produced});
    }
  }

  handleConsumedChange = (event) => {
    this.setState({consumed: event.target.value});
  }

  handleProducedChange = (event) => {
    this.setState({produced: event.target.value});
  }

  handleConsumedSubmit = (event) => {
    this.props.sendMeterValue(this.state.consumed, 0);
    event.preventDefault();
  }

  handleProducedSubmit = (event) => {
    this.props.sendMeterValue(this.state.produced, 1);
    event.preventDefault();
  }

  render() {
    const { fetching, isProducer } = this.props;

    if(fetching) {
      return <div className="loadingIndicator"></div>
    }

    const producerComponent = isProducer ? (
        <form onSubmit={this.handleProducedSubmit}>
          <h3>Olen tuottanut</h3>
          <FormGroup
            controlId="produceForm"
          >
            <FormControl
              type="text"
              value={this.state.produced}
              onChange={this.handleProducedChange}
              placeholder="15"
            />
            <HelpBlock>kWh</HelpBlock>
          </FormGroup>
          <Button type="submit" value="Submit" bsStyle="primary" bsSize="large">Tallenna</Button>
        </form>
    ): null;

    return (
      <div className="SettingsPage">
        {!this.props.loading &&
          <Grid>
            <Row className="show-grid">
              <Col md={12} sm={12} xs={12}>
                <h1 className="center">Sähkömittari</h1>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col md={6} sm={6} xs={6}>
                <form onSubmit={this.handleConsumedSubmit}>
                  <h3>Olen kuluttanut</h3>
                  <FormGroup
                    controlId="consumeForm"
                  >
                    <FormControl
                      type="text"
                      value={this.state.consumed}
                      onChange={this.handleConsumedChange}
                      placeholder="5"
                    />
                    <HelpBlock>kWh</HelpBlock>
                  </FormGroup>
                  <Button type="submit" value="Submit" bsStyle="primary" bsSize="large">Tallenna</Button>
                </form>
              </Col>
              <Col md={6} sm={6} xs={6}>
                {producerComponent}
              </Col>
            </Row>
          </Grid>
        }
      </div>
    );
  }
}

SettingsPage.propTypes = {
  sendMeterValue: PropTypes.func.isRequired,
  registerSuccess: PropTypes.bool,
  loading: PropTypes.bool
}

export default SettingsPage;
