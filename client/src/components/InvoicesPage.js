import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import PropTypes from 'prop-types';

class InvoicesPage extends Component {
  componentWillMount() {
    this.props.fetchInvoices();
  }
  componentDidMount() {
    this.interval = setInterval(this.update, 5000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  update = () => {
    this.props.fetchBackground();
  }
  render() {
    return (
      <div className="ProducerList">
        {!this.props.loading &&
          <Grid>
            <Row className="show-grid">
              <Col md={12} sm={12} xs={12}>
                <h1 className="center">Laskut</h1>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col md={12} sm={12} xs={12}>
                <Table bordered condensed hover>
                  <thead>
                    <tr>
                      <th>Sähköntuottaja</th>
                      <th>Hinta per kWh</th>
                      <th>Määrä</th>
                      <th>Laskun summa</th>
                    </tr>
                  </thead>
                  {!!this.props.invoices &&
                    this.props.invoices.map((invoice, index) => {
                      return (
                        <tbody key={"invoice_" + index}>
                          <tr>
                            <td>{invoice.message.producer}</td>
                            <td>{invoice.message.price} €/kWh</td>
                            <td>{invoice.mosaic.quantity / 1000.0} kWh</td>
                            <td>{invoice.message.price * (invoice.mosaic.quantity / 1000.0)} €</td>
                          </tr>
                        </tbody>
                      )
                    })
                  }
                </Table>
              </Col>
            </Row>
          </Grid>
        }
      </div>
    );
  }
}

InvoicesPage.propTypes = {
  fetchInvoices: PropTypes.func,
  fetchBackground: PropTypes.func,
  invoices: PropTypes.array,
  loading: PropTypes.bool.isRequired
}

export default InvoicesPage;
