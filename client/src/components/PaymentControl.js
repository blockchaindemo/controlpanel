import React, { Component } from 'react';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import { FormGroup, FormControl, HelpBlock } from 'react-bootstrap';
import PropTypes from 'prop-types';

class PaymentControl extends Component {
  constructor(props) {
    super(props);
    this.state = {buyprice: this.props.buyprice, sellprice: this.props.sellprice};
    this.handleBuyPriceChange = this.handleBuyPriceChange.bind(this);
    this.handleBuyPriceSubmit = this.handleBuyPriceSubmit.bind(this);
    this.handleSellPriceChange = this.handleSellPriceChange.bind(this);
    this.handleSellPriceSubmit = this.handleSellPriceSubmit.bind(this);
  }

  componentWillMount() {
    this.props.fetchEnergyPrices();
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.buyprice !== nextProps.buyprice || this.state.sellprice !== nextProps.sellprice) {
      this.setState({buyprice: nextProps.buyprice, sellprice: nextProps.sellprice});
    }
  }

  handleBuyPriceChange(event) {
    this.setState({buyprice: event.target.value});
  }

  handleSellPriceChange(event) {
    this.setState({sellprice: event.target.value});
  }

  handleBuyPriceSubmit(event) {
    this.props.sendPriceValue(this.state.buyprice, 0);
    event.preventDefault();
  }

  handleSellPriceSubmit(event) {
    this.props.sendPriceValue(this.state.sellprice, 1);
    event.preventDefault();
  }

  render() {
    return (
      <div className="PaymentControl">
        {(!this.props.loading &&
          <Grid>
            <Row className="show-grid">
              <Col md={12} sm={12} xs={12}>
                <h1 className="center">Millä hinnalla haluat ostaa sähköä?</h1>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col md={4} sm={3} xs={1} />
              <Col md={4} sm={6} xs={10}>
                <form onSubmit={this.handleBuyPriceSubmit}>
                  <FormGroup
                    controlId="buyForm"
                  >
                    <FormControl
                      type="text"
                      value={this.state.buyprice}
                      onChange={this.handleBuyPriceChange}
                      placeholder="0.10"
                    />
                    <HelpBlock>€/kWh</HelpBlock>
                  </FormGroup>
                  <Button type="submit" value="Submit" bsStyle="primary" bsSize="large">Tallenna</Button>
                </form>
              </Col>
              <Col md={4} sm={3} xs={1} />
            </Row>
            {!!this.props.isproducer &&
              <div>
                <hr />
                <Row className="show-grid">
                  <Col md={12} sm={12} xs={12}>
                    <h1 className="center">Millä hinnalla haluat myydä sähköä?</h1>
                  </Col>
                </Row>
                <Row className="show-grid">
                  <Col md={4} sm={3} xs={1} />
                  <Col md={4} sm={6} xs={10}>
                    <form onSubmit={this.handleSellPriceSubmit}>
                      <FormGroup
                        controlId="sellForm"
                      >
                        <FormControl
                          type="text"
                          value={this.state.sellprice}
                          onChange={this.handleSellPriceChange}
                          placeholder="0.10"
                        />
                        <HelpBlock>€/kWh</HelpBlock>
                      </FormGroup>
                      <Button type="submit" value="Submit" bsStyle="primary" bsSize="large">Tallenna</Button>
                    </form>
                  </Col>
                  <Col md={4} sm={3} xs={1} />
                </Row>
              </div>
            }
            <Row className="show-grid averagePrice">
              <Col md={12} sm={12} xs={12}>
                <h2 className="center">24h keskihinta: {this.props.averagePricePerEnergy}</h2>
              </Col>
            </Row>
          </Grid>
        )}
      </div>
    );
  }
}

PaymentControl.propTypes = {
  sendPriceValue: PropTypes.func,
  fetchEnergyPrices: PropTypes.func,
  buyprice: PropTypes.number.isRequired,
  sellprice: PropTypes.number.isRequired,
  averagePricePerEnergy: PropTypes.number,
  isproducer: PropTypes.bool,
  loading: PropTypes.bool.isRequired
}


export default PaymentControl;
