import React, { Component } from 'react';
import { PageHeader } from 'react-bootstrap';
import PageContainer from './containers/PageContainer';
import { connect } from 'react-redux';
import './styles/App.scss';
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
  render() {
    const header = !this.props.fetching ? (<div className="mainheader">
    <PageHeader className="center">Blockchaindemo control panel</PageHeader>
  </div>) : null;
    return (
      <div className="App">
        {header}
        <PageContainer />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  fetching: state.user.fetching
});

export default connect(mapStateToProps)(App);