const INITIAL_STATE = {
  fetching: false,
  producerList: [],
  error: undefined
};

const producers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'PRODUCER_LIST_REQUEST':
      return {...state, fetching: true};
    case 'PRODUCER_LIST_REQUEST_SUCCESS':
      return {...state, ...action.data, fetching: false, error: undefined}
    case 'PRODUCER_LIST_REQUEST_FAILURE':
      return {...state, ...INITIAL_STATE, fetching: false, error: action.error}
    default:
      return state;
  }
}

export default producers;