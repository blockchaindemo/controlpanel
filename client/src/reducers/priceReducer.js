const INITIAL_STATE = {
  fetching: false,
  buyprice: 0.0,
  sellprice: 0.0,
  averageCostPerEnergy: undefined,
  error: undefined
};

const price = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'ENERGY_PRICE_REQUEST':
      return {...state, fetching: true};
    case 'ENERGY_PRICE_REQUEST_SUCCESS':
      return {...state, ...action.data, fetching: false, error: undefined}
    case 'ENERGY_PRICE_REQUEST_FAILURE':
      return {...INITIAL_STATE, fetching: false, error: action.error}
    case 'SEND_PRICE_REQUEST':
      return {...state, fetching: false};
    case 'SEND_PRICE_REQUEST_SUCCESS':
      return {...state, ...action.data, fetching: false, error: undefined}
    case 'SEND_PRICE_REQUEST_FAILURE':
      return {...INITIAL_STATE, fetching: false, error: action.error}
    default:
      return state;
  }
}

export default price;