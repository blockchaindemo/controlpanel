const INITIAL_STATE = {
  loggedIn: false,
  pubKey: '',
  _id: '',
  consumed: 0.0,
  produced: 0.0,
  isproducer: false,
  fetching: false,
  error: undefined,
  registerSuccess: false,
  loginOrRegisterState: ''
};

const user = (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case 'LOGIN_REQUEST':
      return {...state, fetching: true};
    case 'LOGIN_REQUEST_SUCCESS':
      return {...state, ...action.data.user, fetching: false, error: undefined, loggedIn: true,
        loginOrRegisterState: "login_success"};
    case 'LOGIN_REQUEST_FAILURE':
      return {...state, ...INITIAL_STATE, fetching: false, error: action.error,
        loginOrRegisterState: "login_fail"
      };
    case 'REGISTER_REQUEST':
      return {...state, fetching: true};
    case 'REGISTER_REQUEST_SUCCESS':
      return {...state, ...action.data.user, fetching: false, error: undefined, loggedIn: true, registerSuccess: true, loginOrRegisterState: "register_success"};
    case 'REGISTER_REQUEST_FAILURE':
      return {...state, ...INITIAL_STATE, fetching: false, error: action.error,
        loginOrRegisterState: "register_fail"};
    case 'SEND_METER_REQUEST':
      return {...state, fetching: true};
    case 'SEND_METER_REQUEST_SUCCESS_IN':
      return {...state, consumed: action.data.consumed, fetching: false, error: undefined}
    case 'SEND_METER_REQUEST_SUCCESS_OUT':
      return {...state, produced: action.data.produced, fetching: false, error: undefined}    
    case 'SEND_METER_REQUEST_FAILURE':
      return {...state, fetching: false, error: action.error}
    default:
      return state;
  }
}

export default user;