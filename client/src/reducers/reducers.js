import { combineReducers } from 'redux';
import user from './userReducer';
import price from './priceReducer';
import producers from './producerListReducer';
import invoice from './invoiceReducer';

const mainReducer = combineReducers({
  user,
  price,
  producers,
  invoice
});

export default mainReducer;