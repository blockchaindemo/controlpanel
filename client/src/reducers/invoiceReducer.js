const INITIAL_STATE = {
  fetching: false,
  invoices: [],
  error: undefined
};

const invoice = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'INVOICE_REQUEST':
      return {...state, fetching: true};
    case 'INVOICE_REQUEST_SUCCESS':
      return {...state, invoices: action.data.invoices, fetching: false, error: undefined}
    case 'INVOICE_REQUEST_FAILURE':
      return {...INITIAL_STATE, fetching: false, error: action.error}
    default:
      return state;
  }
}

export default invoice;