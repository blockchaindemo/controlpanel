import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import mainReducer from './reducers/reducers';
import logger from 'redux-logger'


export const mainStore = createStore(
  mainReducer,
  applyMiddleware(thunk, logger)
);