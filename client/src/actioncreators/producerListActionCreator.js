import { mainStore } from '../Store';

export const fetchProducerList = (background) => {
  return (dispatch) => {
    if (!background) {
      dispatch({
        type: 'PRODUCER_LIST_REQUEST'
      })
    }
    return fetch(`/producers?_id=${mainStore.getState().user._id}`,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      .then(response => response.json().then(body => ({ response, body })))
      .then(({ response, body }) => {
        if (!response.ok || !body.success || body.success === "false") {
          dispatch({
            type: 'PRODUCER_LIST_REQUEST_FAILURE',
            error: body.error
          });
        } else {
          dispatch({
            type: 'PRODUCER_LIST_REQUEST_SUCCESS',
            data: body
          });
        }
      })
      .catch(error => {
        dispatch({
          type: 'PRODUCER_LIST_REQUEST_FAILURE',
          error: error
        });
      });
  }
}