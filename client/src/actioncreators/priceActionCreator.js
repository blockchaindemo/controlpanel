import { mainStore } from '../Store';

export const fetchEnergyPrices = () => {
  return (dispatch) => {
    dispatch({
      type: 'ENERGY_PRICE_REQUEST'
    })
    return fetch(`/price?_id=${mainStore.getState().user._id}`,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      .then(response => response.json().then(body => ({ response, body })))
      .then(({ response, body }) => {
        if (!response.ok || !body.success || body.success === "false") {
          dispatch({
            type: 'ENERGY_PRICE_REQUEST_FAILURE',
            error: body.error
          });
        } else {
          dispatch({
            type: 'ENERGY_PRICE_REQUEST_SUCCESS',
            data: body
          });
        }
      })
      .catch(error => {
        dispatch({
          type: 'ENERGY_PRICE_REQUEST_FAILURE',
          error: error
        });
      });
  }
}

export const sendPriceValue = (pricePerEnergy, sell) => {
  return (dispatch) => {
    dispatch({
      type: 'SEND_PRICE_REQUEST'
    })
    return fetch("/price/" + (!sell ? "buy" : "sell"),
      {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify( {_id: mainStore.getState().user._id, pricePerEnergy: pricePerEnergy} )
      })
      .then(response => response.json().then(body => ({ response, body })))
      .then(({ response, body }) => {
        if (!response.ok || !body.success || body.success === "false") {
          dispatch({
            type: 'SEND_PRICE_REQUEST_FAILURE',
            error: body.error
          });
        } else {
          dispatch({
            type: 'SEND_PRICE_REQUEST_SUCCESS',
            data: body
          });
        }
      })
      .catch(error => {
        dispatch({
          type: 'SEND_PRICE_REQUEST_FAILURE',
          error: error
        });
      });
  }
}