export const loginClick = (username, password) => {
  return (dispatch) => {
    dispatch({
      type: 'LOGIN_REQUEST'
    })
    return fetch('/login',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify( {username: username, password: password} )
      })
      .then(response => response.json().then(body => ({ response, body })))
      .then(({ response, body }) => {
        if (!response.ok || body.error) {
          dispatch({
            type: 'LOGIN_REQUEST_FAILURE',
            error: body.error
          });
        } else {
          dispatch({
            type: 'LOGIN_REQUEST_SUCCESS',
            data: body
          });
        }
      })
      .catch(error => {
        dispatch({
          type: 'LOGIN_REQUEST_FAILURE',
          error: error
        });
      });
  }
}

export const registerClick = (username, password, isproducer) => {
  return (dispatch) => {
    dispatch({
      type: 'REGISTER_REQUEST'
    })
    return fetch('/register',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify( {username: username, password: password, isproducer: isproducer} )
      })
      .then(response => response.json().then(body => ({ response, body })))
      .then(({ response, body }) => {
        if (!response.ok || body.error) {
          dispatch({
            type: 'REGISTER_REQUEST_FAILURE',
            error: body.error
          });
        } else {
          dispatch({
            type: 'REGISTER_REQUEST_SUCCESS',
            data: body
          });
        }
      })
      .catch(error => {
        dispatch({
          type: 'REGISTER_REQUEST_FAILURE',
          error: error
        });
      });
  }
}