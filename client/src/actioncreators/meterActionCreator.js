import { mainStore } from '../Store';

export const sendMeterValue = (meterValue, out) => {
  return (dispatch) => {
    dispatch({
      type: 'SEND_METER_REQUEST'
    })
    return fetch("/meter/" + (!out ? "in" : "out"),
      {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify( {_id: mainStore.getState().user._id, meterValue: meterValue} )
      })
      .then(response => response.json().then(body => ({ response, body })))
      .then(({ response, body }) => {
        if (!response.ok || !body.success || body.success === "false") {
          dispatch({
            type: 'SEND_METER_REQUEST_FAILURE',
            error: body.error
          });
        } else {
          dispatch({
            type: 'SEND_METER_REQUEST_SUCCESS_' + (!out ? "IN" : "OUT"),
            data: body
          });
        }
      })
      .catch(error => {
        dispatch({
          type: 'SEND_METER_REQUEST_FAILURE',
          error: error
        });
      });
  }
}