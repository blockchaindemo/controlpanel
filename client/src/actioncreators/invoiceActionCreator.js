import { mainStore } from '../Store';

export const fetchInvoices = (background) => {
  return (dispatch) => {
    if (!background) {
      dispatch({
        type: 'INVOICE_REQUEST'
      })
    }
    return fetch(`/getinvoices?_id=${mainStore.getState().user._id}`,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      .then(response => response.json().then(body => ({ response, body })))
      .then(({ response, body }) => {
        if (!response.ok || !body.success || body.success === "false") {
          dispatch({
            type: 'INVOICE_REQUESTT_FAILURE',
            error: body.error
          });
        } else {
          dispatch({
            type: 'INVOICE_REQUEST_SUCCESS',
            data: body
          });
        }
      })
      .catch(error => {
        dispatch({
          type: 'INVOICE_REQUEST_FAILURE',
          error: error
        });
      });
  }
}