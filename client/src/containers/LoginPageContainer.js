import { connect } from 'react-redux';
import LoginPage from '../components/LoginPage';
import { loginClick, registerClick } from '../actioncreators/userActionCreator';

const mapStateToProps = state => {
  return {
    registerSuccess: state.user.registerSuccess,
    loginOrRegisterState: state.user.loginOrRegisterState,
    fetching: state.user.fetching,
    error: state.user.error
  }
};

const mapDispatchToProps = dispatch => {
  return {
    loginClick: (username, password) => {
      dispatch(loginClick(username, password));
    },
    registerClick: (username, password, wantsell) => {
      dispatch(registerClick(username, password, wantsell));
    }
  }
}

const LoginPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

export default LoginPageContainer;