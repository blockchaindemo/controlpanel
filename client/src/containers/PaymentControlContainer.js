import { connect } from 'react-redux';
import PaymentControl from '../components/PaymentControl';
import { sendPriceValue, fetchEnergyPrices } from '../actioncreators/priceActionCreator';

const mapStateToProps = state => {
  return {
    ...state.price,
    isproducer: state.user.isproducer,
    loading: state.user.fetching || state.price.fetching
  }
};

const mapDispatchToProps = dispatch => {
  return {
    sendPriceValue: (value, sell) => {
      dispatch(sendPriceValue(value, sell));
    },
    fetchEnergyPrices: () => {
      dispatch(fetchEnergyPrices());
    }
  }
}

const PaymentControlContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentControl);

export default PaymentControlContainer;