import { connect } from 'react-redux';
import ProducerList from '../components/ProducerList';
import { fetchProducerList } from '../actioncreators/producerListActionCreator';

const mapStateToProps = state => {
  return {
    ...state.producers,
    loading: state.user.fetching || state.producers.fetching
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchProducerList: () => {
      dispatch(fetchProducerList(0));
    },
    fetchBackground: () => {
      dispatch(fetchProducerList(1));
    }
  }
}
const ProducerListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProducerList);

export default ProducerListContainer;