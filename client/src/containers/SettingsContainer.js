import { connect } from 'react-redux';
import SettingsPage from '../components/SettingsPage';
import { sendMeterValue } from '../actioncreators/meterActionCreator';

const mapStateToProps = state => {
  return {
    isProducer: state.user.isproducer,
    produced: state.user.produced,
    consumed: state.user.consumed,
    loggedIn: state.user.loggedIn
  }
};

const mapDispatchToProps = dispatch => {
  return {
    sendMeterValue: (value, out) => {
      dispatch(sendMeterValue(value, out));
    }
  }
}

const SettingsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsPage);

export default SettingsContainer;