import { connect } from 'react-redux';
import InvoicesPage from '../components/InvoicesPage';
import { fetchInvoices } from '../actioncreators/invoiceActionCreator';

const mapStateToProps = state => {
  return {
    loading: state.invoice.fetching,
    invoices: state.invoice.invoices
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchInvoices: () => {
      dispatch(fetchInvoices(0));
    },
    fetchBackground: () => {
      dispatch(fetchInvoices(1));
    }
  }
}

const InvoicesContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(InvoicesPage);

export default InvoicesContainer;