import { connect } from 'react-redux';
import Page from '../components/Page';

const mapStateToProps = state => {
  return {
    loggedIn: state.user.loggedIn
  }
};

const mapDispatchToProps = dispatch => {
  return {
  }
}

const PageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);

export default PageContainer;